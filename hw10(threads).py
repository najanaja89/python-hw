from threading import Thread
import time
import random


def randomizer():
    return random.randint(1, 10)


class ThreadGenerator(Thread):
    def __init__(self, name):
        super().__init__()
        self.name = name

    def run(self):
        rand = randomizer()
        time.sleep(rand)
        print(f"sleep {rand}")
        print(f"Thread {self.name}")


def create_threads(num):
    for i in range(num):
        th = f"Threads {(i + 1)}"
        thGen = ThreadGenerator(th)
        thGen.start()


create_threads(5)
