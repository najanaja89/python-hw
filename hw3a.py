import os
import argparse

parser = argparse.ArgumentParser(description='ls analog')
parser.add_argument('-l', '--long', action='store_true')
myArgs = parser.parse_args()

#print(myArgs)

myDir = os.walk(os.getcwd())
if myArgs.long:
    for d in myDir:
        path, folders, files = d
        print(f'{path}\t{folders}\t{files}')
        break

else:
    print(os.listdir())
