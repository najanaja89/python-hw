import socket, sys, os
import queue

os.system('cls' if os.name == 'nt' else 'clear')

url = str("""
        <header>
        <h1>My Site</h1>
        </header>
        <html>
        <body>
        <h1>Hello World</h1> this is my server!
        </body>
        </html>
    """)


def parse_request(request):
    requestList = request.split()
    return requestList


def generate_header(method, url):
    if url == 'index.html':
        header = str(f'HTTP/1.0 200 OK\r\n')
    else:
        header = str(f'HTTP/1.0 404 Not Found\r\n')
    return header


def generate_content(code, content=""):
    content = [code, content]
    return content


def generate_response(request):
    content = generate_content('OK', str(url))
    header = generate_header('GET', request)
    return str(content[1] + '\n' + str(header))


server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind(('localhost', 9090))
server_socket.listen()

while True:
    print("Server Started")
    client_socket, addr = server_socket.accept()
    request = client_socket.recv(4096)

    print(str(request, encoding='UTF-8'))

    response = generate_response(request.decode('UTF-8'))
    client_socket.sendall(bytes(response, encoding="UTF-8"))
    client_socket.close()

# if __name__=='__main__':
#     run()
