import collections

print('Task 1')

a = ('a', 'b', '1', '2', 3,)

print(a[1])

Car = collections.namedtuple('Car', 'color mileage')
my_car = Car('yellow', 3.18)

print(my_car.color)
print(my_car.mileage)

print('Task 2')

my_set = {'say', 'hello', 'to', 'my', 'little', 'friend'}
my_frozen_set = frozenset({'say', 'hello'})
print(my_set)
print(my_frozen_set)

words = ['hello', 'daddy', 'hello', 'mum']

print(set(words))

print('Task 3')
d1 = {'d1': 1, 'd2': 2}
print(d1)
print(d1['d1'])

d = dict.fromkeys(['a', 'b'], 100)
print(d)
