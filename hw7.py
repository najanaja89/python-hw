import csv
import pickle
import shelve

# Работа с файлами

with open('test.txt', 'w') as somefile:
    somefile.write('hello world')

# текстовые файлы

# дозапись в файл
with open("test.txt", "a") as file:
    file.write("\ngood bye, world")

# через print()
with open("test.txt", "a") as file:
    print("Hello, world\n", file=file)

# чтение
with open('test.txt', 'r', encoding='utf8') as file:
    for line in file:
        print(line, end="")

# csv

FILENAME = 'users.csv'

users = [
    ['Tom', 28],
    ['Alice', 23],
    ['Bob', 34]
]

with open(FILENAME, 'w', newline="") as file:
    writer = csv.writer(file)
    writer.writerow(users)

with open(FILENAME, "a", newline="") as file:
    user = ["Sam", 31]
    writer = csv.writer(file)
    writer.writerow(user)

with open(FILENAME, "r", newline="") as file:
    reader = csv.reader(file)
    for row in reader:
        print(row[0], ' - ', row[1])

# бинарные файлы
FILENAME = "user.dat"

name = 'Tom'
age = 19

with open(FILENAME, 'wb') as file:
    pickle.dump(name, file)
    pickle.dump(age, file)

with open(FILENAME, 'rb') as file:
    name = pickle.load(file)
    age = pickle.load(file)
    print('Name', name, '\tAge:', age)

users = [
    ["Tom", 28, True],
    ["Alice", 23, False],
    ["Bob", 34, False]
]

with open(FILENAME, "wb") as file:
    pickle.dump(users, file)

with open(FILENAME, "rb") as file:
    users_from_file = pickle.load(file)
    for user in users_from_file:
        print("Имя:", user[0], "\tВозраст:", user[1], "\tЖенат(замужем):", user[2])

# shelve

FILENAME = 'states2'

with shelve.open(FILENAME) as states:
    states["London"] = "Great Britain"
    states["Paris"] = "France"
    states["Berlin"] = "Germany"
    states["Madrid"] = "Spain"

with shelve.open(FILENAME) as states:
    print(states["London"])
    print(states['Madrid'])

with shelve.open(FILENAME) as states:
    state = states.get("Brussels", "Undefined")
    print(state)

with shelve.open(FILENAME) as states:
    state = states.pop("London", "NotFound")
    print(state)

with shelve.open(FILENAME) as states:
    del states["Madrid"]
