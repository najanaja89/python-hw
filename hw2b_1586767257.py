import random

print('task 1')

guess_me = random.randint(1, 15)

if guess_me > 7:
    print('too high')
elif guess_me < 7:
    print('too low')
else:
    print('just right')

print('task2')

start = 1

while True:
    if start < guess_me:
        print("too low")
        start += 1
    elif start > guess_me:
        print('oops')
        start -= 1
        break
    else:
        print('found it!')
        break

print('task 3')

for x in [3, 2, 1, 4]:
    print(x)

print('task 4')


def good():
    return ['Harry', 'Ron', 'Hermione']


print(good())

print('5')

try:
    number = int(input("Enter number: "))
except ValueError:
    print("Oops")
print('end exception')

print('Task 6')
print('enter x')
y = int(input())


print((lambda z: z * z)(y))
