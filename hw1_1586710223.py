print('task 1')
print('input a')
a = float(input())

print('input b')
b = float(input())

import math

print('input c')
c = float(input())

D = (b * b) - (4 * a * c)

if D > 0:
    x1 = ((-b) + math.sqrt((b * b) - (4 * a * c))) / 2 * a
    x2 = ((-b) - math.sqrt((b * b) - (4 * a * c))) / 2 * a
    print('x1:' + str(x1) + '\n' + 'x2:' + str(x2))
elif D < 0:
    print('no X')
else:
    x = -(b / 2 * a)
    print('x:' + str(x))

print('task 2')

print('Enter R')
r = float(input())
S = math.pi * (r * r)

print('S=' + str(S))

print('task 3')
print('Enter string')

myString = input()
myString = list(myString)
print(f'3 element {myString[2]} and last element {myString[-1]}')

print('task 4')
print('Enter string')
myString = input()
print(myString.upper())
print(myString.lower())
print(myString.capitalize())

print('task 5')
print(help('float'))
print(help('int'))
print(help('string'))
print(help('bool'))

print('task 6')
print(dir(2))
print(dir('string'))
print(dir(2.6))
print(dir(True))



