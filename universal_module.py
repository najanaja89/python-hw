import os
import heapq
import collections
import sys
import argparse

if __name__ == "__main__":
    def myDequeCol(maxlen=3):
        q = collections.deque(maxlen)
        return q


    def myHeapqLarges(lastEl, col):
        return heapq.nlargest(lastEl, col)


    def myHeapqSamlles(lastEl, col):
        return heapq.nsmallest(lastEl, col)


    def myDefDict(col):
        return collections.defaultdict(col)


    def myCommomElDict(dictA, dictB):
        return dictA.keys() - dictB.keys()


    def myCounter(col, num):
        myCounter = collections.Counter(col)
        return myCounter.most_common(num)


    def myChainMap(*args):
        return collections.ChainMap(*args)


    def myStringEnd(string="", pattern=""):
        return string.endswith(pattern)


    def myStringContains(string="", contains=""):
        return string.startswith(contains)
