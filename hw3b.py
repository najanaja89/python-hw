class Car:

# Инкапсуляция скрываем свойства класса
    def __init__(self, model, color):
        self.__model = model
        self.__color = color
        self.__engine = 1

    @property
    def engine(self):
        return self.__engine

    @engine.setter
    def engine(self, engine):
        if engine in range(0, 5):
            self.__engine = engine
        else:
            print('wrong parameter')

    @property
    def color(self):
        return self.__color

    @property
    def model(self):
        return self.__model

    def display_info(self):
        print(f"Model: {self.model}\nColor: {self.color}\nEngine: {self.engine}")


#Наследование, наследуемся от класса Car
class SuperCar(Car):

#Инкапсуляция скрываем свойства класса
    def __init__(self, model, color):
        super().__init__(model, color)
        self.__nitro = 1

    @property
    def nitro(self):
        return self.__nitro

    @nitro.setter
    def nitro(self, nitro):
        self.__nitro = nitro

#Полиморфизм переопределяем метод родительского класса
    def display_info(self):
        Car.display_info(self)
        print(f'Nitro: {self.nitro}')


myCar = Car('Toyota', 'red')
mySCar = SuperCar('Porshe', 'gray')
myCar.engine = 4
mySCar.nitro = 3
myCar.display_info()
mySCar.display_info()
