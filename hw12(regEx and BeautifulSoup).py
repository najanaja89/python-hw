import requests
import json
from bs4 import BeautifulSoup
import re

myJson = requests.get("https://jsonplaceholder.typicode.com/comments").json()

with open('myJson.json', 'w') as write_file:
    json.dump(myJson, write_file)

page = requests.get("https://habrahabr.ru")

soup = BeautifulSoup(page.content)

links = [link.get('href') for link in soup.find_all('a', attrs={'href': re.compile("^http://")})]

print(links)

