import socket

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(('localhost', 9090))

client.sendall(bytes("index.html", encoding='UTF8'))
data = client.recv(4096)

client.close()

print(data.decode())
