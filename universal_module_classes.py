from socket import socket, AF_INET, SOCK_STREAM
from functools import partial
from abc import ABCMeta, abstractmethod
import math


if __name__ == "__main__":

    class MyDate:
        def __init__(self, year, month, day):
            self.year = year
            self.month = month
            self.day = day
            self.formats = _formats = {
                'ymd': '{d.year}-{d.month}-{d.day}',
                'mdy': '{d.month}/{d.day}/{d.year}',
                'dmy': '{d.day}/{d.month}/{d.year}'
            }

        def __format__(self, code):
            if code == '':
                code = 'ymd'
            fmt = self.formats[code]
            return fmt.format(d=self)


    class MyPair:
        def __init__(self, x, y):
            self.x = x
            self.y = y

        def __repr__(self):
            return 'Pair({0.x!r}, {0.y!r})'.format(self)

        def __str__(self):
            return '({0.x!s}, {0.y!s})'.format(self)


    class MyLazyConnection:
        def __init__(self, address, family=AF_INET, type=SOCK_STREAM):
            self.address = address
            self.family = AF_INET
            self.type = SOCK_STREAM
            self.sock = None

        def __enter__(self):
            if self.sock is not None:
                raise RuntimeError('Already connected')
            self.sock = socket(self.family, self.type)
            self.sock.connect(self.address)
            return self.sock

        def __exit__(self, exc_ty, exc_val, tb):
            self.sock.close()
            self.sock = None


    class MyDateSlots:
        __slots__ = ['year', 'month', 'day']

        def __init__(self, year, month, day):
            self.year = year
            self.month = month
            self.day = day


    class MyPerson:
        def __init__(self, first_name):
            self.first_name = first_name

        @property
        def first_name(self):
            return self._first_name

        @first_name.setter
        def first_name(self, value):
            if not isinstance(value, str):
                raise TypeError('Expected a string')
            self._first_name = value

        @first_name.deleter
        def first_name(self):
            raise AttributeError("Can't delete attribute")

        class MyInteger:
            def __init__(self, name):
                self.name = name

            def __get__(self, instance, cls):
                if instance is None:
                    return self
                else:
                    return instance.__dict__[self.name]

            def __set__(self, instance, value):
                if not isinstance(value, int):
                    raise TypeError('Expected an int')
                instance.__dict__[self.name] = value

            def __delete__(self, instance):
                del instance.__dict__[self.name]

        class MyIStream(metaclass=ABCMeta):
            @abstractmethod
            def read(self, maxbytes=-1):
                pass

            @abstractmethod
            def write(self, data):
                pass

        class MyPoint:
            def __init__(self, x, y):
                self.x = x
                self.y = y

            def __repr__(self):
                return 'Point({!r:},{!r:})'.format(self.x, self.y)

            def distance(self, x, y):
                return math.hypot(self.x - x, self.y - y)


